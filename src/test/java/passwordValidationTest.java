/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author grant
 */
public class passwordValidationTest {
    
    public passwordValidationTest() {
    }
    
    @BeforeEach
    public void setUp() {
    }

    /**
     * Test of checkValid method, of class passwordValidation.
     */
    @Test
    public void testIsValidPasswordDigitRegular() {
        boolean expR=true, result;
        System.out.println("IsValidPasswordDigitRegular");
        String p = "Welcome16!";
        passwordValidation instance = new passwordValidation();
        result=instance.checkValid(p);
        assertEquals(expR,result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
     @Test
    public void testIsValidPasswordDigitEceptional() {
        boolean expR=false, result;
        System.out.println("IsValidPasswordDigitRegular");
        String p = "Weleeeee!";
        passwordValidation instance = new passwordValidation();
        result=instance.checkValid(p);
        assertEquals(expR,result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
     @Test
    public void testIsValidPasswordDigitBoundaryIn() {
        boolean expR=true, result;
        System.out.println("IsValidPasswordDigitRegular");
        String p = "Welcome1!";
        passwordValidation instance = new passwordValidation();
        result=instance.checkValid(p);
        assertEquals(expR,result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
     @Test
     public void testIsValidPasswordDigitBoundaryOut() {
        boolean expR=false, result;
        System.out.println("IsValidPasswordDigitRegular");
        String p = "Welcome!";
        passwordValidation instance = new passwordValidation();
        result=instance.checkValid(p);
        assertEquals(expR,result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
     
     
     
     
     
     
      @Test
     public void testIsValidPasswordLengthRegular() {
        boolean expR=true, result;
        System.out.println("IsValidPasswordDigitRegular");
        String p = "Welcome16!";
        passwordValidation instance = new passwordValidation();
        result=instance.checkValid(p);
        assertEquals(expR,result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
     @Test
    public void testIsValidPasswordLengthEceptional() {
        boolean expR=false, result;
        System.out.println("IsValidPasswordDigitRegular");
        String p = "Wel9!";
        passwordValidation instance = new passwordValidation();
        result=instance.checkValid(p);
        assertEquals(expR,result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
     @Test
    public void testIsValidPasswordLengthBoundaryIn() {
        boolean expR=true, result;
        System.out.println("IsValidPasswordDigitRegular");
        String p = "Welcome1!";
        passwordValidation instance = new passwordValidation();
        result=instance.checkValid(p);
        assertEquals(expR,result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
     @Test
     public void testIsValidPasswordLengthBoundaryOut() {
        boolean expR=false, result;
        System.out.println("IsValidPasswordDigitRegular");
        String p = "Welc16!";
        passwordValidation instance = new passwordValidation();
        result=instance.checkValid(p);
        assertEquals(expR,result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
     
     
     
     
      @Test
     public void testIsValidPasswordUpperRegular() {
        boolean expR=true, result;
        System.out.println("IsValidPasswordDigitRegular");
        String p = "WWWelcome16!";
        passwordValidation instance = new passwordValidation();
        result=instance.checkValid(p);
        assertEquals(expR,result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
     @Test
    public void testIsValidPasswordUpperEceptional() {
        boolean expR=false, result;
        System.out.println("IsValidPasswordDigitRegular");
        String p = "weeeeel9!";
        passwordValidation instance = new passwordValidation();
        result=instance.checkValid(p);
        assertEquals(expR,result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
     @Test
    public void testIsValidPasswordUpperBoundaryIn() {
        boolean expR=true, result;
        System.out.println("IsValidPasswordDigitRegular");
        String p = "Welcome1!";
        passwordValidation instance = new passwordValidation();
        result=instance.checkValid(p);
        assertEquals(expR,result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
     @Test
     public void testIsValidPasswordUpperBoundaryOut() {
        boolean expR=false, result;
        System.out.println("IsValidPasswordDigitRegular");
        String p = "weeeeeelc16!";
        passwordValidation instance = new passwordValidation();
        result=instance.checkValid(p);
        assertEquals(expR,result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
     
      @Test
     public void testIsValidPasswordSpecialRegular() {
        boolean expR=true, result;
        System.out.println("IsValidPasswordDigitRegular");
        String p = "Welcome16!";
        passwordValidation instance = new passwordValidation();
        result=instance.checkValid(p);
        assertEquals(expR,result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
     @Test
    public void testIsValidPasswordSpecialEceptional() {
        boolean expR=false, result;
        System.out.println("IsValidPasswordDigitRegular");
        String p = "weeeeel9";
        passwordValidation instance = new passwordValidation();
        result=instance.checkValid(p);
        assertEquals(expR,result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
     @Test
    public void testIsValidPasswordSpecialBoundaryIn() {
        boolean expR=true, result;
        System.out.println("IsValidPasswordDigitRegular");
        String p = "Welcome1!";
        passwordValidation instance = new passwordValidation();
        result=instance.checkValid(p);
        assertEquals(expR,result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
     @Test
     public void testIsValidPasswordSpecialBoundaryOut() {
        boolean expR=false, result;
        System.out.println("IsValidPasswordDigitRegular");
        String p = "weeeeeelc16";
        passwordValidation instance = new passwordValidation();
        result=instance.checkValid(p);
        assertEquals(expR,result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
     
     
    
}
